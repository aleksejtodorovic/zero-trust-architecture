import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import Home from './pages/Home';
import Login from './pages/Login';

function App() {
    return (
        <Router>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/login' component={Login} />
                <Route path='/'>
                    <Redirect to='/' />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
