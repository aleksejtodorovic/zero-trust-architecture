import axios from 'axios';

export const checkCreds = async ({ email, password }) => {
    try {
        // return {
        //     success: true
        // };

        const response = await axios.post('/api/login?step=1', {
            email,
            password
        });

        return response?.data;
    } catch (ex) {
        return {
            error: 'Oooops, something went wrong'
        };
    }
};

export const checkAuthCode = async ({ code }) => {
    try {
        const response = await axios.post('/api/login?step=2', {
            code: `Bearer ${code}`
        });

        return response?.data;
    } catch (ex) {
        return {
            error: 'Oooops, something went wrong'
        };
    }
};

export const closeSession = (code) => {
    try {
        axios.post('/api/logout', { code });
    } catch (ex) {
        console.log('Logout ex', ex);
    }
};
