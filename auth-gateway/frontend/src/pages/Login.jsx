import React, { useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';

// API's
import { checkCreds, checkAuthCode } from '../apis/auth';

// context
import { StateContext } from '../context/state';

function Login() {
    const {
        state: { loggedIn },
        login
    } = useContext(StateContext);
    const [step, setStep] = useState(1);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [code, setCode] = useState('');
    const [error, setError] = useState('');

    if (loggedIn) {
        return <Redirect to='/' />;
    }

    const checkCredentials = async () => {
        const response = await checkCreds({ email, password });

        if (response?.success) {
            setError('');
            setStep(2);
            setCode(response.code);
        } else if (response?.error) {
            setError(response.error);
        }
    };

    const checkCode = async () => {
        const response = await checkAuthCode({ code });

        if (response?.services && response?.code) {
            login(response);
        } else if (response?.error) {
            setError(response.error);
        }
    };

    if (step === 1) {
        return (
            <MDBContainer className='mt-5 pt-5'>
                <MDBRow className='d-flex align-items-center justify-content-center'>
                    <MDBCol md='6'>
                        <form
                            onSubmit={(evt) => {
                                evt.preventDefault();
                                checkCredentials();
                            }}
                        >
                            <p className='h2 text-center mb-4'>Sign in</p>
                            <div className='grey-text'>
                                <MDBInput
                                    label='Type your email'
                                    icon='envelope'
                                    group
                                    type='email'
                                    validate
                                    error='wrong'
                                    success='right'
                                    value={email}
                                    onChange={({ target: { value } }) => setEmail(value)}
                                />
                                <MDBInput
                                    label='Type your password'
                                    icon='lock'
                                    c
                                    group
                                    type='password'
                                    validate
                                    value={password}
                                    onChange={({ target: { value } }) =>
                                        setPassword(value)
                                    }
                                />
                            </div>
                            <div className='text-center'>
                                {error && (
                                    <p className='font-weight-bold text-danger'>
                                        {error}
                                    </p>
                                )}
                                <MDBBtn
                                    color='secondary'
                                    disabled={!email || !password}
                                    type='submit'
                                >
                                    Submit
                                </MDBBtn>
                            </div>
                        </form>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    } else {
        return (
            <MDBContainer className='mt-5'>
                <MDBRow className='d-flex align-items-center justify-content-center'>
                    <MDBCol md='6'>
                        <form
                            onSubmit={(evt) => {
                                evt.preventDefault();
                                checkCode();
                            }}
                        >
                            <p className='h2 text-center mb-4'>Sign in - Step 2</p>
                            <div className='grey-text'>
                                <MDBInput
                                    label='Type your code'
                                    icon='key'
                                    group
                                    type='text'
                                    validate
                                    error='wrong'
                                    success='right'
                                    value={code}
                                    onChange={({ target: { value } }) => setCode(value)}
                                />
                            </div>
                            <div className='text-center'>
                                {error && (
                                    <p className='font-weight-bold text-danger'>
                                        {error}
                                    </p>
                                )}
                                <MDBBtn color='secondary' disabled={!code} type='submit'>
                                    Submit
                                </MDBBtn>
                            </div>
                        </form>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    }
}

export default Login;
