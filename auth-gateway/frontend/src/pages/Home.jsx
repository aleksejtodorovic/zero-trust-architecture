import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import {
    MDBContainer,
    MDBTable,
    MDBRow,
    MDBCol,
    MDBTableBody,
    MDBTableHead,
    MDBBtn
} from 'mdbreact';

// context
import { StateContext } from '../context/state';

// API's
import { closeSession } from '../apis/auth';

function Home() {
    const {
        state: { loggedIn, services, code },
        logout
    } = useContext(StateContext);

    if (!loggedIn) {
        return <Redirect to='/login' />;
    }

    const renderServices = () => {
        return services.map((service, index) => {
            return (
                <tr>
                    <td>{index}</td>
                    <td>{service.name}</td>
                    <td>{service.ip}</td>
                    <td>{service.port}</td>
                </tr>
            );
        });
    };

    const onLogout = () => {
        closeSession(code);
        logout();
    };

    return (
        <MDBContainer className='mt-5 pt-5'>
            <MDBRow className='d-flex align-items-center justify-content-center'>
                <MDBCol md='10'>
                    <div className='d-flex justify-content-between'>
                        <h1>Available Services</h1>
                        <MDBBtn
                            className='left-0'
                            color='secondary'
                            onClick={() => onLogout()}
                        >
                            Logout
                        </MDBBtn>
                    </div>
                    <MDBTable hover>
                        <MDBTableHead>
                            <tr>
                                <th>#</th>
                                <th>Protocol</th>
                                <th>IP</th>
                                <th>Port</th>
                            </tr>
                        </MDBTableHead>
                        <MDBTableBody>{renderServices()}</MDBTableBody>
                    </MDBTable>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
}

export default Home;
