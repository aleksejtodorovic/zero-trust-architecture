// libs
import createDataContext from './createDataContext';

const initialState = {
    loggedIn: false,
    code: null,
    services: []
};

const stationReducer = (state, { type, payload }) => {
    switch (type) {
        case 'LOGIN':
            return {
                ...state,
                loggedIn: true,
                services: payload.services,
                code: payload.code
            };
        case 'LOGOUT':
            return { ...state, services: {}, loggedIn: false, code: null };
        default:
            return state;
    }
};

const login = (dispatch) => ({ code, services }) => {
    dispatch({ type: 'LOGIN', payload: { code, services } });
};

const logout = (dispatch) => async () => {
    dispatch({ type: 'LOGOUT' });
};

export const { Context: StateContext, Provider: StateProvider } = createDataContext(
    stationReducer,
    {
        login,
        logout
    },
    initialState
);
