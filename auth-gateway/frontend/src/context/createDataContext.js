import React, { createContext, useReducer, useMemo } from 'react';

const createDataContext = (reducer, actions, initialState) => {
    const Context = createContext();

    const Provider = (props) => {
        const [state, dispatch] = useReducer(reducer, initialState);
        const boundActions = {};

        for (let key in actions) {
            boundActions[key] = actions[key](dispatch);
        }

        const memoizedActions = useMemo(() => boundActions, []);

        return <Context.Provider value={{ state, ...memoizedActions }} {...props} />;
    };

    return { Context, Provider };
};

export default createDataContext;
