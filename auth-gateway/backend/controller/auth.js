const { sendMessage } = require('../socket');

const login = async (req, res, next) => {
  const { step } = req.query;

  if (step === '1') {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(200).json({ error: 'Credentials not provided' });
    }

    try {
      const response = await sendMessage({ message: 'check-credentials', data: { email, password, ip: req.header('x-forwarded-for') } });

      if (response.error) {
        return res.status(200).json({ error: response.error });
      }

      // TODO: Send code to email

      return res.status(200).json({
        success: true,
        code: response.code
      });
    } catch (ex) {
      console.log('ex', ex);
      return res.status(200).json({ error: 'Internal server error' });
    }
  } else if (step === '2') {
    if (req.body.code) {
      const response = await sendMessage({ message: 'code-check', data: { code: req.body.code, ip: req.header('x-forwarded-for') } });

      if (response.error) {
        return res.status(200).json({
          error: response.error
        });
      } else {
        return res.status(200).json({
          code: req.body.code,
          services: response.services
        });
      }
    } else {
      return res.status(200).json({ error: 'Code is not valid' });
    }
  }

  return res.status(200).json({ error: `Step ${step} not valid!` });
};

const logout = async (req, res, next) => {
  sendMessage({ message: 'logout', data: { ip: req.header('x-forwarded-for') } });

  return res.status(200).json({ message: 'Session clossed.' });
  // } else {
  //   return res.status(200).json({ error: `Code not valid!` });
  // }
};

module.exports = {
  login,
  logout
};
