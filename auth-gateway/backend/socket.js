let serverSocket;

const startServerSocket = (server) => {
  const io = require('socket.io')(server);

  io.on('connection', (socket) => {
    serverSocket = socket;
  });
};

const sendMessage = ({ message, data }) => {
  return new Promise((resolve, reject) => {
    if (serverSocket) {
      if (message === 'check-credentials') {
        serverSocket.once(`credentials-result-${data.ip}`, (response) => {
          resolve(response);
        });

        serverSocket.emit(message, data);
      } else if (message === 'code-check') {
        serverSocket.once(`code-result-${data.ip}`, (response) => {
          resolve(response);
        });

        serverSocket.emit(message, data);
      } else {
        serverSocket.emit(message, data);
        resolve();
      }
    } else {
      reject('Server socket missing!');
    }
  });
};

module.exports = {
  startServerSocket,
  sendMessage
};
