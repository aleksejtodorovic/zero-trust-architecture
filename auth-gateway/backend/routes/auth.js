const { Router } = require('express');

// controllers
const { login, logout } = require('../controller/auth');

const router = Router();

router.post('/login', login);

router.post('/logout', logout);

module.exports = router;
