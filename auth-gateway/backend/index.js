const app = require('express')();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const { startServerSocket } = require('./socket');

// routes
const authRoutes = require('./routes/auth');

app.use(bodyParser.json());

app.use('/', authRoutes);

app.use((error, req, res, next) => {
    try {
        const { data, message } = error;

        res.status(200).json({
            error: message,
            data
        });
    } catch (ex) {
        console.log(ex);
    }
});

startServerSocket(server);

server.listen(process.env.PORT || 3000, () => {
    console.log(`Auth Gateway Backend server listen on port ${process.env.PORT || 3000}`);
});
