const { Schema, model } = require('mongoose');

const UserSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  services: {
    type: [
      {
        name: {
          type: String,
          required: true
        },
        ip: {
          type: String,
          required: true
        },
        port: {
          type: String,
          required: true
        }
      }
    ],
    default: []
  }
});

module.exports = model('User', UserSchema);
