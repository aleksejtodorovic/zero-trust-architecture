const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const { getRedisSet, getRedisGet } = require('./redis-client');

// models
const User = require('./model/User');

const JWT_KEY = process.env.JWT_KEY || 'strongsecretkey';

let socket;

const decodeToken = ({ code, ip }) => {
    try {
        const token = code.split(' ')[1];
        const decodedCode = jwt.verify(token, JWT_KEY);

        if (decodedCode?.ip === ip) {
            return decodedCode;
        }

        return false;
    } catch (ex) {
        return false;
    }
};

const connectToAuth = async () => {
    socket = require('socket.io-client')(
        `ws://${process.env.SOCKET_SERVER_URI}:${process.env.SOCKET_SERVER_PORT}`
    );

    // handle the event sent with socket.send()
    socket.on('check-credentials', async ({ email, password, ip }) => {
        const user = await User.findOne({ email });
        const emitMessage = `credentials-result-${ip}`;

        if (!user) {
            socket.emit(emitMessage, { error: 'Email does not exists!' });
            return;
        }

        const isEqual = await bcrypt.compare(password, user.password);

        if (!isEqual) {
            socket.emit(emitMessage, { error: 'Wrong password!' });
            return;
        }

        const code = jwt.sign(
            {
                email: user.email,
                ip
            },
            JWT_KEY,
            { expiresIn: '1h' }
        );

        socket.emit(emitMessage, { code });
    });

    // handle the event sent with socket.emit()
    socket.on('code-check', async ({ code, ip }) => {
        const emitMessage = `code-result-${ip}`;
        const decodedToken = decodeToken({ code, ip });

        if (decodedToken) {
            try {
                const user = await User.findOne({ email: decodedToken.email });
                const serviceArray = [];

                user.services.forEach((service) => {
                    serviceArray.push(service.name);
                });

                const response = await axios.post(
                    `http://${process.env.PORT_CONFIGURATOR_URI}:${process.env.PORT_CONFIGURATOR_PORT}/add`,
                    {
                        ip,
                        services: serviceArray
                    }
                );

                if (response.data.status != 'OK') {
                    socket.emit(emitMessage, { error: 'Internal server error !' });
                } else {
                    await getRedisSet()(ip, JSON.stringify(serviceArray));
                    socket.emit(emitMessage, { services: user.services });
                }
            } catch (ex) {
                console.log('ex', ex);
                socket.emit(emitMessage, { error: 'Internal server error!' });
            }
        } else {
            socket.emit(emitMessage, { error: 'Code is not valid!' });
        }
    });

    socket.on('logout', async ({ ip }) => {
        const response = await getRedisGet()(ip);
        const parsedResponse = JSON.parse(response);

        await axios.post(
            `http://${process.env.PORT_CONFIGURATOR_URI}:${process.env.PORT_CONFIGURATOR_PORT}/remove`,
            {
                ip,
                services: parsedResponse
            }
        );

        await getRedisSet()(ip, JSON.stringify([]));
    });
};

module.exports = {
    connectToAuth
};
