const redis = require('redis');
const { promisify } = require('util');

let redisClient;
var setAsync;
var getAsync;

const connectToRedis = async () => {
    return new Promise((resolve, reject) => {
        redisClient = redis.createClient({
            host: process.env.REDIS_URI
        });

        redisClient.on('ready', () => {
            console.log('connected to redis');
            setAsync = promisify(redisClient.set).bind(redisClient);
            getAsync = promisify(redisClient.get).bind(redisClient);

            resolve();
        });

        redisClient.on('error', (error) => {
            reject(error);
        });
    });
};

const getRedisSet = () => {
    return setAsync;
};

const getRedisGet = () => {
    return getAsync;
};

module.exports = {
    connectToRedis,
    getRedisSet,
    getRedisGet
};
