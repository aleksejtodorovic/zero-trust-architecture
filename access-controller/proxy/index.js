const net = require('net');
const axios = require('axios');
const proxywrap = require('findhit-proxywrap');
const mongoose = require('mongoose');

const { getRedisGet, connectToRedis } = require('./redis-client');
const { connectToAuth } = require('./socket');

mongoose
    .connect(
        `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_URI}/platform?authSource=admin&w=majority&readPreference=primary`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }
    )
    .then(async () => {
        await connectToRedis();
        connectToAuth();
    })
    .catch((ex) => {
        console.log(ex);
    });

const HTTP_REMOTE_ADDR = process.env.HTTP_SERVICE_URI;
const HTTP_LOCAL_PORT = process.env.HTTP_LOCAL_PORT;
const HTTP_REMOTE_PORT = process.env.HTTP_REMOTE_PORT;

const SSH_REMOTE_ADDR = process.env.SSH_SERVICE_URI;
const SSH_LOCAL_PORT = process.env.SSH_LOCAL_PORT;
const SSH_REMOTE_PORT = process.env.SSH_REMOTE_PORT;

const FTP_REMOTE_ADDR = process.env.FTP_SERVICE_URI;
const FTP_ACTIVE_PORT = process.env.FTP_ACTIVE_PORT;
const FTP_PASSIVE_PORT_START = process.env.FTP_PASSIVE_PORT_START;
const FTP_PASSIVE_PORT_END = process.env.FTP_PASSIVE_PORT_END;

const hasAccess = async ({ ip, protocol }) => {
    const response = await getRedisGet()(ip);
    const parsedResponse = JSON.parse(response);

    return parsedResponse && parsedResponse.includes(protocol);
};

const closeServicesPorts = async ({ ip, services }) => {
    await axios.post(
        `http://${process.env.PORT_CONFIGURATOR_URI}:${process.env.PORT_CONFIGURATOR_PORT}/remove`,
        {
            ip,
            services
        }
    );
};

// HTTP
proxywrap
    .proxy(net)
    .createServer(async (localsocket) => {
        const userIp = localsocket.remoteAddress;
        const accessEnabled = await hasAccess({ ip: userIp, protocol: 'http' });

        if (!accessEnabled) {
            localsocket.end();
            closeServicesPorts({
                ip: userIp,
                services: ['http']
            });

            return;
        }

        const remotesocket = new net.Socket();

        remotesocket.connect(HTTP_REMOTE_PORT, HTTP_REMOTE_ADDR);

        localsocket.on('data', function (data) {
            const flushed = remotesocket.write(data);

            if (!flushed) {
                console.log('  remote not flushed; pausing local');
                localsocket.pause();
            }
        });

        remotesocket.on('data', function (data) {
            const flushed = localsocket.write(data);

            if (!flushed) {
                console.log('  local not flushed; pausing remote');
                remotesocket.pause();
            }
        });

        localsocket.on('drain', function () {
            remotesocket.resume();
        });

        remotesocket.on('drain', function () {
            localsocket.resume();
        });

        localsocket.on('close', function (had_error) {
            remotesocket.end();
        });

        remotesocket.on('close', function (had_error) {
            localsocket.end();
        });
    })
    .listen(HTTP_LOCAL_PORT);

// SSH
proxywrap
    .proxy(net)
    .createServer(async (localsocket) => {
        const userIp = localsocket.remoteAddress;
        const accessEnabled = await hasAccess({ ip: userIp, protocol: 'ssh' });

        if (!accessEnabled) {
            localsocket.end();
            closeServicesPorts({
                ip: userIp,
                services: ['ssh']
            });

            return;
        }

        const remotesocket = new net.Socket();

        remotesocket.connect(SSH_REMOTE_PORT, SSH_REMOTE_ADDR);

        localsocket.on('data', function (data) {
            const flushed = remotesocket.write(data);

            if (!flushed) {
                console.log('  remote not flushed; pausing local');
                localsocket.pause();
            }
        });

        remotesocket.on('data', function (data) {
            const flushed = localsocket.write(data);

            if (!flushed) {
                console.log('  local not flushed; pausing remote');
                remotesocket.pause();
            }
        });

        localsocket.on('drain', function () {
            remotesocket.resume();
        });

        remotesocket.on('drain', function () {
            localsocket.resume();
        });

        localsocket.on('close', function (had_error) {
            remotesocket.end();
        });

        remotesocket.on('close', function (had_error) {
            localsocket.end();
        });
    })
    .listen(SSH_LOCAL_PORT);

// FTP active port
proxywrap
    .proxy(net)
    .createServer(async (localsocket) => {
        const userIp = localsocket.remoteAddress;
        const accessEnabled = await hasAccess({ ip: userIp, protocol: 'ftp' });

        if (!accessEnabled) {
            localsocket.end();
            closeServicesPorts({
                ip: userIp,
                services: ['ftp']
            });

            return;
        }

        const remotesocket = new net.Socket();

        remotesocket.connect(FTP_ACTIVE_PORT, FTP_REMOTE_ADDR);

        localsocket.on('data', function (data) {
            const flushed = remotesocket.write(data);

            if (!flushed) {
                console.log('  remote not flushed; pausing local');
                localsocket.pause();
            }
        });

        remotesocket.on('data', function (data) {
            const flushed = localsocket.write(data);

            if (!flushed) {
                console.log('  local not flushed; pausing remote');
                remotesocket.pause();
            }
        });

        localsocket.on('drain', function () {
            remotesocket.resume();
        });

        remotesocket.on('drain', function () {
            localsocket.resume();
        });

        localsocket.on('close', function (had_error) {
            remotesocket.end();
        });

        remotesocket.on('close', function (had_error) {
            localsocket.end();
        });
    })
    .listen(FTP_ACTIVE_PORT);

// FTP passive ports
for (let port = FTP_PASSIVE_PORT_START; port <= FTP_PASSIVE_PORT_END; ++port) {
    proxywrap
        .proxy(net)
        .createServer(async (localsocket) => {
            const userIp = localsocket.remoteAddress;
            const accessEnabled = await hasAccess({ ip: userIp, protocol: 'ftp' });

            if (!accessEnabled) {
                localsocket.end();
                closeServicesPorts({
                    ip: userIp,
                    services: ['ftp']
                });

                return;
            }

            const remotesocket = new net.Socket();

            remotesocket.connect(port, FTP_REMOTE_ADDR);

            localsocket.on('data', function (data) {
                const flushed = remotesocket.write(data);

                if (!flushed) {
                    console.log('  remote not flushed; pausing local');
                    localsocket.pause();
                }
            });

            remotesocket.on('data', function (data) {
                const flushed = localsocket.write(data);

                if (!flushed) {
                    console.log('  local not flushed; pausing remote');
                    remotesocket.pause();
                }
            });

            localsocket.on('drain', function () {
                remotesocket.resume();
            });

            remotesocket.on('drain', function () {
                localsocket.resume();
            });

            localsocket.on('close', function (had_error) {
                remotesocket.end();
            });

            remotesocket.on('close', function (had_error) {
                localsocket.end();
            });
        })
        .listen(port);
}

console.log('TCP server accepting connection on ports');
