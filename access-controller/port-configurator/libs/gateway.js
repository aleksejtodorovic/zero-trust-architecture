const axios = require('axios');

const gatewayAPI = axios.create({
    baseURL: `http://${process.env.GATEWAY_URI}:${process.env.GATEWAY_PORT}/v2/services/haproxy`,
    auth: {
        username: process.env.GATEWAY_USERNAME,
        password: process.env.GATEWAY_PASSWORD
    }
});

const SERVICES = {
    HTTP: 'http',
    SSH: 'ssh',
    FTP: 'ftp'
};

const createTransaction = async (version) => {
    if (!version) {
        const frontendResponse = await gatewayAPI.get(`/configuration/frontends/http`);
        version = frontendResponse.data._version;
    }

    const transaction = await gatewayAPI.post(`/transactions?version=${version}`);

    return transaction;
};

const saveTransaction = async (id) => {
    await gatewayAPI.put(`/transactions/${id}`);
};

const addRules = async (services, ip) => {
    try {
        if (services.length < 1) {
            return;
        }

        const rulesPromises = [];
        const transaction = await createTransaction();

        services.forEach((service) => {
            rulesPromises.push(
                gatewayAPI.post(
                    `/configuration/acls?parent_name=${service}&parent_type=frontend&transaction_id=${transaction.data.id}`,
                    {
                        acl_name: 'network_allowed',
                        value: ip,
                        criterion: 'src',
                        index: 0
                    }
                )
            );
        });

        Promise.all(rulesPromises)
            .then(async () => {
                await saveTransaction(transaction.data.id);
            })
            .catch((ex) => {
                throw ex;
            });
    } catch (ex) {
        throw ex;
    }
};

const deleteRules = async (services, ip) => {
    try {
        if (services.length < 1) {
            return;
        }

        let promises = [];

        services.forEach((service) => {
            promises.push(
                gatewayAPI.get(
                    `/configuration/acls?parent_name=${service}&parent_type=frontend`
                )
            );
        });

        Promise.all(promises)
            .then(async (serviceAclsResponses) => {
                promises = [];

                // take version of first in response array, it is same in all of them
                const transaction = await createTransaction(
                    serviceAclsResponses[0]?.data._version
                );

                const indexesInAcls = [];

                serviceAclsResponses.forEach((serviceAclResponse, index) => {
                    indexesInAcls.push({
                        service: services[index],
                        index: serviceAclResponse.data.data.findIndex(
                            (acl) => acl.value === ip
                        )
                    });
                });

                indexesInAcls.forEach((aclIndexObject) => {
                    if (aclIndexObject.index >= 0) {
                        promises.push(
                            gatewayAPI.delete(
                                `/configuration/acls/${aclIndexObject.index}?parent_name=${aclIndexObject.service}&parent_type=frontend&transaction_id=${transaction.data.id}`
                            )
                        );
                    }
                });

                Promise.all(promises)
                    .then(async () => {
                        await saveTransaction(transaction.data.id);
                    })
                    .catch((ex) => {
                        throw ex;
                    });
            })
            .catch((ex) => {
                throw ex;
            });
    } catch (ex) {
        throw ex;
    }
};

module.exports = {
    SERVICES,
    addRules,
    deleteRules
};
