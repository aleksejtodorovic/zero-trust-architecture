const logger = require('simple-node-logger').createSimpleFileLogger({
    logFilePath: 'logs/logs.log',
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
});

exports.log = ({ ip, message }) => {
    logger.info(`${ip} - ${message}`);
};
