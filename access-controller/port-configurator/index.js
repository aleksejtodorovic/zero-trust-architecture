const express = require('express');
const bodyParser = require('body-parser');

// libs
const gatewayAPI = require('./libs/gateway');
const { log } = require('./libs/logger');

const app = express();
app.use(bodyParser.json());

app.use('/remove', async (req, res) => {
    try {
        const { ip, services } = req.body;

        await gatewayAPI.deleteRules(services, ip);

        services.forEach((service) => {
            log({
                ip,
                message: `${service} PORT REMOVED`
            });
        });

        res.status(200).json({
            status: 'OK',
            message: 'REMOVE API CALLED'
        });
    } catch (ex) {
        res.status(200).json({
            status: 'ERROR',
            message: ex.message
        });
    }
});

app.use('/add', async (req, res) => {
    try {
        const { ip, services } = req.body;

        await gatewayAPI.addRules(services, ip);

        services.forEach((service) => {
            log({
                ip,
                message: `${service} PORT APPROVED`
            });
        });

        res.status(200).json({
            status: 'OK',
            message: 'ADD API CALLED'
        });
    } catch (ex) {
        res.status(200).json({
            status: 'ERROR',
            message: ex.message
        });
    }
});

app.listen(process.env.PORT || 3000, () => {
    console.log(`PORT CONFIGURATOR server listen on port ${process.env.PORT || 3000}`);
});
