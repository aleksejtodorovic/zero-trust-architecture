const { Router } = require('express');

// controllers
const { createTodo, getTodos, updateTodo, deleteTodo } = require('../controller/todo');

const router = Router();

router.post('/', createTodo);

router.get('/', getTodos);

router.patch('/:id', updateTodo);

router.delete('/:id', deleteTodo);

module.exports = router;
