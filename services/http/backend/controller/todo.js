// models
const Todo = require('../model/Todo');

const createTodo = async (req, res, next) => {
    const { title, description, done } = req.body;
    const newTodo = await new Todo({
        title,
        description,
        done
    });

    await newTodo.save();

    res.status(201).json({ todo: newTodo });
};

const getTodos = async (req, res, next) => {
    const todos = await Todo.find({});

    res.status(200).json({ todos });
};

const updateTodo = async (req, res, next) => {
    const { id } = req.params;
    const { title, description, done } = req.body;
    const todo = await Todo.findById(id);

    if (!todo) {
        return req.status(404).json({ message: 'Post not found!' });
    }

    todo.title = title || todo.title;
    todo.description = description || todo.description;
    todo.done = typeof done === 'boolean' ? done : todo.done;

    await todo.save();

    res.status(200).json({ message: 'Upated!', updatedTodo: todo });
};

const deleteTodo = async (req, res, next) => {
    const { id } = req.params;

    const todo = await Todo.findById(id);

    if (!todo) {
        return req.status(404).json({ message: 'Post not found!' });
    }

    await Todo.findByIdAndRemove(id);

    res.status(200).json({ message: 'Todo deleted!' });
};

module.exports = {
    createTodo,
    getTodos,
    updateTodo,
    deleteTodo
};
