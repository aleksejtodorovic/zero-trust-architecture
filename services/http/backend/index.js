const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// routes
const todoRoutes = require('./routes/todo');

const app = express();

app.use(bodyParser.json());

app.use('/', todoRoutes);

mongoose
    .connect(
        `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_URI}?retryWrites=true&w=majority`,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }
    )
    .then(() => {
        app.listen(process.env.PORT || 3000, () => {
            console.log(`HTTP server listen on port ${process.env.PORT || 3000}`);
        });
    })
    .catch((ex) => {
        console.log(ex);
    });
